extern crate server;
use server::Server;

fn main() {
    let server = Server::new(String::from("127.0.0.1:7878"), 4);
    server.start();
}
