mod threadpool;
use threadpool::ThreadPool;

use std::fs;
use std::io::prelude::*;
use std::net::TcpStream;
use std::net::TcpListener;

pub struct Server {
    address: String,
    pool: ThreadPool,
}

impl Server {
    pub fn new(address: String, workers: usize) -> Server{
        let pool = ThreadPool::new(workers);
        Server { address, pool }
    }

    pub fn start(&self) {
        let listener = TcpListener::bind(&self.address).unwrap();
        for stream in listener.incoming() {
            let stream = stream.unwrap();
            self.pool.execute(|| {
                Server::handle_connection(stream);
            });
        }
    }

    fn handle_connection(mut stream: TcpStream) {
        let mut buffer = [0; 512];
        stream.read(&mut buffer).unwrap();

        let get = b"GET / HTTP/1.1\r\n";

        let (status_line, filename) = if buffer.starts_with(get) {
            ("HTTP/1.1 200 OK\r\n\r\n", "static/index.html")
        } else {
            ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "static/404.html")
        };

        let contents = fs::read_to_string(filename).unwrap();

        let response = format!("{}{}", status_line, contents);

        stream.write(response.as_bytes()).unwrap();
        stream.flush().unwrap();
    }
}